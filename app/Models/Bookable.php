<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bookable extends Model
{
    use HasFactory;

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    // if one to many relationship, function should be plural
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    // you can use this function when calling the bookable class
    public function availableFor($from, $to): bool
    {
        return 0 == $this->bookings()->betweenDates($from, $to)->count();
    }
}
