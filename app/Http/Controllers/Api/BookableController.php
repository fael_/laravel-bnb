<?php

namespace App\Http\Controllers\Api;

use App\Models\Bookable;
use App\Http\Resources\BookableShowResource;
use App\Http\Resources\BookableIndexResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookableController extends Controller
{
    // using resources is useful for security as to not return any sensitive data like passwords
    // by having control of what the api/resource returns

    public function index()
    {
        return BookableIndexResource::collection(
            Bookable::all()
        );
    }

    public function show($id)
    {
        return new BookableShowResource(Bookable::findOrFail($id));
    }
}
