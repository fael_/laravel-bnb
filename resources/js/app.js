require('./bootstrap');

import VueRouter from 'vue-router';
import router from './routes';
import index from './index.vue';
import moment from 'moment';
import StarRating from './shared/components/StarRating.vue';
window.Vue = require('vue');

// initiates vue router
Vue.use(VueRouter);

Vue.filter("fromNow", value => moment(value).fromNow())

Vue.component("StarRating", StarRating)

const app = new Vue({
    el: '#app',
    router: router,
    components: {
        'index': index
    }
});

// the app.js file is responsible for handling the connection of routes and components
// this is where several vue features are implemented
