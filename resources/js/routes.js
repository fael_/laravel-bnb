import VueRouter from "vue-router";
import Bookables from "./bookables/Bookables.vue";
import Bookable from "./bookable/Bookable.vue";
import Review from "./review/Review.vue";
const routes = [
    {
        path: "/",
        component: Bookables,
        name: "home",
    },
    {
        path: "/bookable/:id",
        component: Bookable,
        name: "bookable"
    },
    {
        path: "/review/:id",
        component: Review,
        name: "review",
    },

];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
// equivalent to web.php in laravel, but here is where you attach a component to a route
// this file must be exported in order to be rendered in app.js